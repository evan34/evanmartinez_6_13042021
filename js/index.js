import HomeView from './views/HomeView';
import PhotographerView from './views/PhotographerView';
import ParseUrl from './services/ParseUrl';
import Error404 from './components/404';


const router = async () => {
  const routes = {
    '/': new HomeView(),
    '/photographers/:id': new PhotographerView()
  }

  const header = document.querySelector('#header');
  const content = document.querySelector('#main');

  let request = new ParseUrl().parseRequest();
  let parsedUrl = (request.resource ? '/' + request.resource : '/') + (/^[0-9]*$/.test(request.id) ? '/:id' : '');

  if (routes[parsedUrl]) {
    header.innerHTML = await routes[parsedUrl].renderHeader();
    content.innerHTML = await routes[parsedUrl].renderMain();
    await routes[parsedUrl].renderedDom();
    document.querySelector('.header__logo').focus();
  } else {
    header.innerHTML = '';
    content.innerHTML = new Error404().render();
  }



  // let page = await routes[parsedUrl] ? routes[parsedUrl] : new Error404().render();

  // header.innerHTML = await page.renderHeader();
  // content.innerHTML = await page.renderMain();

  // await page.renderedDom();


  // Reload page when photographers are filtered to display all photographers
  if (request.resource === "" || request.resource === "/" || request.resource === null) {
    const reloadPage = () => {
      const logo = document.querySelector('.header__logo');
      logo.addEventListener('click', () => {
        document.location.reload();
      });
      logo.onkeyup = (e) => {
        if (e.key === 'Enter') {
          document.location.reload();
        }
      }
    }
    reloadPage()
  }


}
// Listen on hash change:
window.addEventListener('hashchange', router);

// Listen on page load:
window.addEventListener('load', router);
