/**
 * Class representing header photographer view HTML template 
 */
class HeaderPhotographerView {
  /**
   * 
   * @param { String } tags 
   * @param { Object } data 
   */
  constructor(tags, data) {
    this.tags = tags;
    this.data = data;
  }

  render() {

    document.title = this.data.name;
    const header = document.querySelector('#header');
    return header.innerHTML = `
      <div id="header__container-photographer" class="header__container-photographer">
        <a class="header__logo" href="/#/" tabindex="0" aria-label="Retour vers la page d'accueil ">
          <img src="./img/logo/logo.png" alt="Page d'accueil FishEye">
        </a>
        <section id="header__body" class="header__body">
          <div class="header__body--info">
            <h1 class="info__title" tabindex="0">${this.data.name}</h1>
            <p class="info__city" tabindex="0" aria-label="réside à ${this.data.city}">${this.data.city}</p>
            <p class="info__tagline" tabindex="0" aria-label="slogan: ${this.data.tagline}">${this.data.tagline}</p>
            <ul class="info__tags--ul">
              ${this.tags.map(el => `<li class="info__tags--li" tabindex="0" aria-label="${el}">#${el}</li>`).join("")}
            </ul>
          </div>
          <div class="header__body--contact">
            <button id="contact__button" class="contact__button" type="submit">
              Contactez-moi
            </button>
            <img class="contact__img" src="./img/portraits/${this.data.portrait}"  alt="portrait de ${this.data.name}">
          </div>
        </section>
      </div>
    `;
  }
}

export default HeaderPhotographerView;