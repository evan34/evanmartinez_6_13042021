/**
 * Class representing header home view HTML template 
 */
class HeaderHomeView {
  constructor() {

  }

  render() {

    document.title = 'FishEye | Accueil';

    const tags = [
      '#Portrait',
      '#Art',
      '#Fashion',
      '#Architecture',
      '#Travel',
      '#Sport',
      '#Animals',
      '#Events'
    ];

    const header = document.querySelector('#header');
    return header.innerHTML = `
      <div id="header__container" class="header__container-home">
        <a class="header__logo" href="/#/" tabindex="0" >
          <img src="./img/logo/logo.png" alt="Page d'accueil FishEye">
        </a>
        <nav id="nav" class="header__nav" aria-label="      categorie" tabindex="0">
          <ul class="header__nav--ul">
            ${tags.map(el => `<li data-filter="${el}" class="header__nav--li btn" tabindex="0" aria-label="${el.slice(1)}">${el}</li>`).join("")}
          </ul>
        </nav>
        <h1 id="header__title" class="header__title">Nos photographes</h1>
        <a href="#main" id="header__link" class="header__link">Passer au contenu</a>
      </div>
    `;
  }
}

export default HeaderHomeView;