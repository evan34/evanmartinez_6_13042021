import ParseUrl from '../services/ParseUrl';
import FetchData from '../services/FetchData';
import DropdownSort from '../services/DropdownSort';
import Increment from '../services/Increment';

import HeaderPhotographerView from './HeaderPhotographerView';
import MediaCard from '../components/MediaCard';
import Dropdown from '../components/Dropdown';

import PhotographerInfo from '../components/PhotographerInfos';
import ContactModal from '../components/ContactModal';
import ModalFactory from '../components/ModalFactory';


/**
 * Class representing photographer view HTML template 
 */
class PhotographerView {
  constructor() {
    this.results = [];
    this.request = new ParseUrl().parseRequest();
  }

  /**
   * @param { Number } id 
   * @returns data
   */
  getPhotographerData = async (id) => {
    let data = null;
    const photographerData = await new FetchData('photographers').getData();
    photographerData.map(el => {
      if (el.id === id) {
        data = el;
      }
    });
    return data;
  }

  /**
   * @param { Number } id 
   * @returns tags
   */
  getPhotographerTags = async (id) => {
    let tags = [];
    const photographerData = await new FetchData('photographers').getData();
    photographerData.map(el => {
      if (el.id === id) {
        el.tags.map(tag => {
          tags.push(tag);
        });
      }
    });
    return tags;
  }

  /**
   * @param { Number } id 
   * @returns price
   */
  getPhotographerPrice = async (id) => {
    let price;
    const photographerData = await new FetchData('photographers').getData();
    photographerData.map(el => {
      if (el.id === id) {
        price = el.price;
      }
    });
    return price;
  }

  renderHeader = async () => {
    const data = await this.getPhotographerData(parseInt(this.request.id));
    const tags = await this.getPhotographerTags(parseInt(this.request.id));
    return new HeaderPhotographerView(tags, data).render();
  }

  /**
   * @param { Number } id 
   * @returns String name
   */
  getPhotographerNameById = async (id) => {
    let photographerName = '';
    const photographerData = await new FetchData('photographers').getData();
    photographerData.map(el => {
      if (el.id === id) {
        photographerName = el.name;
      }
    })
    return photographerName;
  }

  /**
   * @param { number } id 
   * @returns { HTMLElement } photographer media list
   */
  getMediaByPhotographerId = async (id) => {
    try {
      const data = await new FetchData('media').getData();
      data.map(el => {
        if (el.photographerId === id) {
          this.results.push(el);
        }
      });
      return this.results;
    } catch (err) {
      console.log(err);
    }
  }

  /**
   * @returns { HTMLElement } main container
   */
  renderMain() {
    const selectElement = document.createElement('section');
    selectElement.classList.add('section');
    selectElement.setAttribute('aria-hidden', false);
    const dropdown = new Dropdown().render();
    const section = document.createElement('section');
    section.id = 'card__section';
    selectElement.innerHTML = dropdown;
    selectElement.append(section);

    return selectElement.outerHTML;
  }

  renderedDom = async () => {
    let card = null;
    let photographerInfo = null;
    let likes = null;
    let totalLikes = [];
    let request = new ParseUrl().parseRequest();
    let name = await this.getPhotographerNameById(parseInt(request.id));
    let data = await this.getMediaByPhotographerId(parseInt(request.id));
    let price = await this.getPhotographerPrice(parseInt(this.request.id));

    data.map(media => {
      card = new MediaCard(media, name).render();
      document.querySelector('#card__section').innerHTML += card;
      totalLikes.push(media.likes);
      likes = media.likes;
    });
    photographerInfo = new PhotographerInfo(totalLikes, price).render();
    document.querySelector('#main').innerHTML += photographerInfo;
    new ContactModal(name).render();
    new ModalFactory('contactForm').render();
    new ModalFactory('lightbox').render();
    new DropdownSort().selectMediaByOption();
    new Increment(likes).render();
  };
}

export default PhotographerView;