import HeaderHomeView from './HeaderHomeView';
import PhotographerCard from '../components/PhotograperCard.js';

import FetchData from '../services/FetchData.js';
import FilterPhotographer from '../services/FilterPhotographer';

/**
 * Class representing home view HTML template 
 */
class HomeView {
  constructor() {

  }

  renderHeader() {
    return new HeaderHomeView().render();
  }

  renderMain() {

    const section = document.createElement('section');
    section.id = 'cardPhotographer__section';
    section.classList.add('cardPhotographer__section');
    section.style.display = "flex";
    section.style.justifyContent = "space-between";

    return section.outerHTML;
  }

  renderedDom = () => {
    const data = new FetchData('photographers').getData();
    let card = null;
    data.then(photographers => {
      photographers.map(photographer => {
        card = new PhotographerCard(photographer).render();
        document.querySelector('#cardPhotographer__section').innerHTML += card;
      });
      new FilterPhotographer().filter()
    })
      .catch(err => {
        console.log(err);
      });
  }
}

export default HomeView;
