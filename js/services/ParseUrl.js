/**
 *  Class representing url parse handler 
 */
class ParseUrl {
  parseRequest = () => {
    let url = location.hash.slice(1).toLowerCase() || '/';
    let res = url.split('/');
    const request = {
      resource: null,
      id: null
    };

    request.resource = res[1] || null
    request.id = res[2] || null
    return request
  }
}

export default ParseUrl;