/**
 * Class representing the likes increment handler
 */
class Increment {
  constructor() {
    this.likes = document.querySelectorAll('.heart');
    this.numberOfLikesEl = document.querySelectorAll('.numberOfLikes');
    this.totalLikes = document.querySelector('.totalLikes');
  }

  /**
   *  @param { Number } index 
   */
  incrementLikes = (index) => {
    // Click on media likes only once
    if (this.numberOfLikesEl[index].dataset.clicked === 'true') {
      return;
    }
    // Increment media card likes
    let nbrOfLikes = parseInt(this.numberOfLikesEl[index].textContent);
    nbrOfLikes++;
    console.log(nbrOfLikes);
    this.numberOfLikesEl[index].setAttribute('data-clicked', 'true');
    this.numberOfLikesEl[index].textContent = nbrOfLikes.toString();
    // Increment the totality media likes
    let allLikes = parseInt(this.totalLikes.textContent);
    allLikes++;
    this.totalLikes.textContent = allLikes;
    console.log(allLikes);
  }

  render() {
    Array.from(this.likes).forEach(el => {
      let index = Array.from(this.likes).indexOf(el);
      el.addEventListener('click', () => this.incrementLikes(index));
      el.onkeyup = (e) => {
        if (e.key === 'Enter') {
          this.incrementLikes(index);
        }
      }
    });
  }
}

export default Increment;