/**
 *  Class representing the json data fetch
 */
class FetchData 
{
  /**
   * @param { String } component media / photographer
   */
  constructor(component) {
    this.component = component;
    this.data = './data.json';
  }

  async getData() {
    try {
      const response = await fetch(this.data)
      const data = await response.json()
  
      switch(this.component) {
        case 'media': return data.media;
        break;
        case 'photographers': return data.photographers;
        break;
      }
    } catch(err) {
      console.log(err);
    }
  }
}

export default FetchData;