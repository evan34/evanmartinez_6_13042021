/**
 *  Class representing a dropdown items sort handler
 */
class DropdownSort {
  constructor() {
    this.selectWrapper = document.querySelector('#select-wrapper');
    this.selectTrigger = document.querySelector('.select__trigger');
    this.options = document.querySelectorAll('.option'); //nodelist
    this.toggleEl = document.querySelector('.select');
  }

  // Functions onClick options object
  selectFunctions = {
    selectByLikes: (a, b) => {
      return b.dataset.likes - a.dataset.likes;
    },
    selectByDate: (a, b) => {
      const firstEl = a.dataset.date.toLowerCase().split("-").join("");
      const nextEl = b.dataset.date.toLowerCase().split("-").join("");

      //return firstEl < nextEl ? -1 : firstEl > nextEl ? 1 : 0;
      return firstEl - nextEl;
    },
    selectByTitle: (a, b) => {
      const firstEl = a.dataset.title.toLowerCase().split("_").join("");
      const nextEl = b.dataset.title.toLowerCase().split("_").join("");

      return firstEl < nextEl ? -1 : firstEl > nextEl ? 1 : 0;
    }
  }

  /**
   *  Select the options HTMLElements and append to DOM
   *  @param { Function } Using sort method to compare array items
   */
  selectElements = currentSort => {
    const section = document.querySelector('#card__section');
    const selected = [...document.querySelectorAll('.card-media')].sort(currentSort);
    console.log(selected);
    selected.forEach(elem => {
      section.appendChild(elem)
    });
  }

  addToggleClass = () => {
    this.toggleEl.classList.toggle('open');
  }

  /**
   * @param { HTMLElement } selectWrapper 
   */
  toggleElements = selectWrapper => {
    selectWrapper.addEventListener('click', this.addToggleClass);
    selectWrapper.onkeyup = (e) => {
      if (e.key === 'Enter') {
        this.addToggleClass();
      }
    }
  }

  /**
   * 
   * @param { HTMLElement } span  
   */
  selectOptions = option => {
    let currentSort = this.selectFunctions.selectByLikes;
    if (!option.classList.contains('selected')) {
      if (option.parentNode.querySelector('.option.selected')) {
        option.parentNode.querySelector('.option.selected').classList.remove('selected');
      }
      let selectSortValue = option.dataset.select;
      switch (selectSortValue) {
        case 'selectByDate':
          currentSort = this.selectFunctions.selectByDate;
          this.selectElements(currentSort);
          break;
        case 'selectByTitle':
          currentSort = this.selectFunctions.selectByTitle;
          this.selectElements(currentSort);
          break;
        case 'selectByLikes':
          currentSort = this.selectFunctions.selectByLikes;
          this.selectElements(currentSort);
          break;
      }
      this.selectTrigger.classList.remove('selected');
      option.classList.add('selected');
      option.closest('.select').querySelector('.select__trigger span').textContent = option.textContent;
    }
  }

  selectMediaByOption() {
    this.toggleElements(this.selectWrapper)
    Array.from(this.options).map(option => {
      this.selectTrigger.classList.add('selected');
      // select the function to display the current elements sort
      option.addEventListener('click', (e) => this.selectOptions(option));
      option.onkeyup = (e) => {
        if (e.key === 'Enter') {
          this.selectOptions(option);
        }
      }
    });
  }
}

export default DropdownSort;