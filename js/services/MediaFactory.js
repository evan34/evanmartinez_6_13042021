class Image {
  constructor() { }
  render() {
    return `
      <img tabindex="0" class="lightbox__img">
    `;
  }
}

class Video {
  constructor() { }
  render() {
    return `
      <video controls type="video/mp4" class="lightbox__img" tabindex="0"></video>
    `;
  }
}

class MediaFactory {
  constructor(type) {
    switch (type) {
      case 'image': return new Image();
      case 'video': return new Video();
    }
  }
}

export default MediaFactory;