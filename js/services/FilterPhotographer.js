/**
 * Class representing photographers sort handler by tag 
 */
class FilterPhotographer {
  constructor() { }

  filter = () => {
    const buttons = document.querySelectorAll('.btn');
    const cards = document.querySelectorAll('.cardPhotographer');
    const section = document.querySelector('#cardPhotographer__section');

    buttons.forEach(b => {
      b.addEventListener('click', e => {
        const filter = e.target.dataset.filter.toLowerCase();
        cards.forEach(c => {
          if (c.classList.contains(filter)) {
            c.style.display = 'block';
            section.style.justifyContent = "center";
          } else {
            c.style.display = 'none';
          }
        });
      });
      b.onkeyup = (e) => {
        if (e.key === 'Enter') {
          const filter = e.target.dataset.filter.toLowerCase();
          cards.forEach(c => {
            if (c.classList.contains(filter)) {
              c.style.display = 'block';
              section.style.justifyContent = "center";
            } else {
              c.style.display = 'none';
            }
          });
        }
      }
    });
  }
}

export default FilterPhotographer;