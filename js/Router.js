import Home from './components/Home';
import PhotographerView from './components/PhotographerView';
import ParseUrl from './services/ParseUrl';

class Router 
{
  constructor() {
    this.request = new ParseUrl().parseRequest();
  }

  router = async () => {
    const routes = {
      '/': new Home().render(),
      '/photographers/:id': new PhotographerView().showData()
    }

    const content = document.getElementById('root');
    
    //let request = new ParseUrl().parseRequest();

    let parsedUrl = (this.request.resource ? '/' + this.request.resource : '/') + (this.request.id ? '/:id' : '') + (this.request.verb ? '/' + this.request.verb : '');
    //console.log(parsedUrl);
    let page = routes[parsedUrl] ? routes[parsedUrl] : Error404
    content.innerHTML = await page;


  }
}

export default Router;
