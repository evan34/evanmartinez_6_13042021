
/**
 *  Class representing a home card photographer html template
 */
class PhotographerCard {
  /**
   * 
   * @param { Object } photographer 
   */
  constructor(photographer) {
    this.photographer = photographer;
  }

  render() {
    const tagsELs = this.photographer.tags;
    const tag = tagsELs.map(el => `#${el}`).join(" ");
    return `
      <div id="card__item" class="cardPhotographer ${tag}" data-id="${this.photographer.id}">
        <a href="#/photographers/${this.photographer.id}" class="cardPhotographer__header">
          <img class="img" src="./img/portraits/${this.photographer.portrait}" alt="portrait de ${this.photographer.name}">
          <h2 class="name" aria-label="nom du photographe: ${this.photographer.name}" tabindex="0">${this.photographer.name}</h2>
        </a>
        <div class="cardPhotographer__body">
          <p class="city" aria-label="réside à ${this.photographer.city}" tabindex="0">${this.photographer.city}</p>
          <p class="tagline" aria-label="slogan: ${this.photographer.tagline}" tabindex="0">${this.photographer.tagline}</p>
          <p class="price" aria-label="tarif: ${this.photographer.price}€ par   jour" tabindex="0">${this.photographer.price}€/jour</p>
        </div>
        <div class="cardPhotographer__footer">
          <ul class="tags" aria-label="liste d'intêrets " tabindex="0">
            ${tagsELs.map(tag => `<li class="tag" aria-label="${tag}" tabindex="0">#${tag}</li>`).join("")}
          </ul>
        </div>
        
      </div>
    `;
  }
}

export default PhotographerCard;