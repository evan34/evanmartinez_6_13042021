/**
 *  Class representing a photographer card media html template
 */
class MediaCard {
  /**
   * 
   * @param { Object[] } media data
   * @param { String } photographerName name
   */
  constructor(media, photographerName) {
    this.media = media;
    this.photographerName = photographerName;
  }

  render() {

    let title = '';
    if (title.lastIndexOf('.jpg') || title.lastIndexOf('.mp4')) {
      this.media.image ?
        title = this.media.image.replace('.jpg', '').split('_').join(' ')
        :
        title = this.media.video.replace('.mp4', '').split('_').join(' ')
    }

    /**
     * Build html image / video media
     * @param { Object[] } media 
     * @returns String
     */
    const checkDataSet = (media = this.media) => {
      let mediaTemplate;
      if (media.image) {
        mediaTemplate = `data-src="img/${this.photographerName}/${this.media.image}"`;
      } else if (media.video) {
        mediaTemplate = `data-src="img/${this.photographerName}/${this.media.video}"`;
      }
      return mediaTemplate;
    }

    /**
     * Build html image / video template card
     * @param { Object[] } media 
     * @returns String
     */
    const checkContainerMedia = (media = this.media) => {
      let containerTemplate;
      if (media.image) {
        containerTemplate =
          `<div class="container__header">
            <img class="header__image"
              loading="lazy"
              tabindex="0"
              src="img/${this.photographerName}/${this.media.image}"
              aria-label="image de ${title} dont le prix est de ${this.media.price} euros et comptabilise ${this.media.likes} likes. Bouton ouvrir lightbox"
              alt="image de ${title} dont le prix est de ${this.media.price} euros et comptabilise ${this.media.likes} likes"
              >
          </div>
          <div class="container__body">
            <h2 class="header__name">${title}</h2>
            <div class="card__info">
              <p class="body__price">${this.media.price} €</p>
              <div class="likesContainer">
                <p class="numberOfLikes">${this.media.likes}</p>
                <span id="heart" class="heart" tabindex="0" aria-label="liker "><i class="fas fa-heart"></i></span>
              </div>
            </div>
          </div>`
      } else if (media.video) {
        containerTemplate =
          `<div class="container__header">
           <video class="header__image"
             loading="lazy"
            tabindex="0"
            src="img/${this.photographerName}/${this.media.video}"
            aria-label="vidéo de ${title} dont le prix est de ${this.media.price} euros et comptabilise ${this.media.likes} likes. Bouton ouvrir lightbox"
            alt="vidéo de ${title} dont le prix est de ${this.media.price} euros et comptabilise ${this.media.likes} likes"
            type="video/mp4"
            >
           </video>
         </div>
         <div class="container__body">  
           <h2 class="header__name">${title}</h2>
           <div class="card__info">
             <p class="body__price">${this.media.price} €</p>
             <div class="likesContainer">
               <p class="numberOfLikes" >${this.media.likes}</p>
               <span id="heart" class="heart" tabindex="0" aria-label="liker "        ><i class="fas fa-heart"></i></span>
             </div>
           </div>
         </div>`;
      }
      return containerTemplate;
    }


    return `
      <div class="card-media" 
            data-date="${this.media.date}" 
            data-likes="${this.media.likes}" 
            data-title="${title}" 
            data-id="${this.media.id}"  
            ${checkDataSet(this.media)}
      >
      ${checkContainerMedia(this.media)}
      </div>
    `;
  }
}

export default MediaCard;