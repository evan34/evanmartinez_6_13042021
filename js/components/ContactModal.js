
/**
 *  Class representing a contact form html template
 */
class ContactModal {
  /**
   * @param { string } name 
   */
  constructor(data) {
    this.data = data;
  }

  render() {
    const main = document.querySelector('#main');
    return main.innerHTML += `
      <div id="contactModal" class="dialog" aria-modal="true" role="dialog" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true"">
        <div class="modalContainer">
          <div class="dialog__header">        
            <h1 class="dialog__header--title">Contactez-moi</h1>
            <button type="button" role="button" class="btn btn-close" data-dismiss="contactModal" aria-label="Fermer le formulaire">Fermer</button>
          </div>
          <div class="dialog__body">
            <h2>${this.data}</h2>
            <form id="form" class="dialog__body--form" method="dialog" action="#">
              <div class="form-input">
                <label for="prenom">Prénom</label>
                <input type="text" name="prenom" id="prenom" aria-label="prenom" aria-required="true" aria-invalid="false">
              </div>
              <div class="form-input">
                <label for="nom">Nom</label>
                <input type="text" name="nom" id="nom" aria-label="nom" aria-required="true" aria-invalid="false">
              </div>
              <div class="form-input">
                <label for="email">Email</label>
                <input type="email" name="email" id="email" aria-label="email" aria-required="true" aria-invalid="false">
              </div>
              <div class="form-input">
                <label for="message">Votre message</label>
                <textarea name="message" id="message" aria-label="Votre message" aria-required="true" aria-invalid="false" rows="6" cols="50"></textarea>
              </div>
              <button class="btn btn-modal" type="submit">
                Envoyer
              </button>
            </form>
          </div>
        </div>
      </div>
    `;
  }
}

export default ContactModal;