/**
 *  Class representing a dropdown html template module
 */
class Dropdown {
  constructor() {

  }

  render() {
    return `
      <div class="select__section">
        <p class="select__title">Trier par</p>
        <div class="select__dropdown">
          <div id="select-wrapper" class="select-wrapper" aria-label="liste de tri" tabindex="0">
            <div class="select">
              <div class="select__trigger"><span>Popularité</span>
              <div class="arrow"></div>
            </div>
            <div class="options">
              <span class="option" data-value="like" data-select="selectByLikes" tabindex="0">Popularité</span>                   
              <span class="option" data-value="date" data-select="selectByDate" tabindex="0">Date</span>
              <span class="option" data-value="titre" data-select="selectByTitle" tabindex="0">Titre</span>
            </div>
            </div>
          </div>
        </div>
      </div>
    `;
  }
}

export default Dropdown;