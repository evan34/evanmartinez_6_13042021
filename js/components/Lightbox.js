/**
 *  Class representing a lightbox html template module
 */
class Lightbox {
  constructor() { }

  render() {
    return `
    <div id="lightbox" class="lightbox">
      <span class="icon fas fa-times close lightbox__closeIcon">X</span>
      <div class="lightbox__content">
        <div class="slide prev"><i class="fas fa-angle-left"></i></div>
        <div class="slide next"><i class="fas fa-angle-right"></i></div>
      </div>
    </div>
    <div class="shadow"></div>
    `;
  }
}

export default Lightbox;