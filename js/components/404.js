/**
 *  Class representing Error template HTML 
 */
class Error404 {
  render() {
    return `
      <section class="error-section">
        <h1>Erreur 404</h1>
        <p>Page non trouvée !</p>
        <div class="home-link">
          <a href="/">Retour vers l'accueil</a>
        </div>
      </section>
    `;
  }
}

export default Error404;