import MediaFactory from '../services/MediaFactory';
/**
 *  Class representing a lightbox handler
 */
class Lightbox {
  constructor() {
    this.createLightboxHtmlElements();
    this.section = document.querySelector('.section');
    this.aside = document.querySelector('aside');
    this.images = document.querySelectorAll('.header__image');
    this.lightboxContainer = document.querySelector('.lightbox__container');
    this.imagesSrc;
    this.alt = '';
    this.src;
    this.nodeName;
  }

  /**
   *  Remove prev image before next image
   */
  removeImage = () => {
    let imageElement = this.lightboxContainer.querySelector('img');
    let videoElement = this.lightboxContainer.querySelector('video');
    if (imageElement) {
      this.lightboxContainer.removeChild(imageElement);
    }
    if (videoElement) {
      this.lightboxContainer.removeChild(videoElement);
    }
  }

  openLightbox = (src, nodeName, alt) => {
    this.lightboxContainer.style.display = 'grid';
    this.lightboxContainer.classList.add('active');
    this.lightboxContainer.setAttribute('aria-hidden', false);
    this.lightboxContainer.setAttribute('aria-modal', true);
    this.section.setAttribute('aria-hidden', true);
    this.aside.setAttribute('aria-hidden', true);
    this.lightboxContainer.focus()
    this.removeImage();
    this.createImage(src, nodeName, alt);
    this.closeLightboxButton();
    this.closeLightboxKey();
    this.handleSlideButtons();
  }

  closeLightbox = () => {
    const lightboxImg = document.querySelector('.lightbox__img');
    this.lightboxContainer.removeChild(lightboxImg);
    this.lightboxContainer.classList.remove('active');
    this.lightboxContainer.setAttribute('aria-hidden', true);
    this.lightboxContainer.setAttribute('aria-modal', false);
    this.lightboxContainer.style.display = 'none';
    this.closeButton.removeEventListener('click', this.closeLightbox);
    this.section.setAttribute('aria-hidden', false);
    this.aside.setAttribute('aria-hidden', false);
    document.removeEventListener('keyup', this.onKeyUp);
    document.removeEventListener('click', this.goNext);
    document.removeEventListener('click', this.goPrev);
  }

  closeLightboxButton = () => {
    this.closeButton = document.querySelector('.icon');
    this.closeButton.addEventListener('click', this.closeLightbox);
  }

  closeLightboxKey = () => {
    this.onKeyUp = this.onKeyUp.bind(this);
    document.addEventListener('keyup', this.onKeyUp);
  }

  goPrev = (e) => {
    e.stopPropagation();

    let index = this.imagesSrc.findIndex(image => image === this.src);
    if (index === 0) {
      index = this.imagesSrc.length;
    }
    this.openLightbox(this.imagesSrc[index - 1], this.images[index - 1].nodeName, this.alt[index - 1]);
  }

  goNext = (e) => {
    e.stopPropagation()
    let index = this.imagesSrc.findIndex(image => image === this.src);
    if (index === this.imagesSrc.length - 1) {
      index = -1;
    }
    this.openLightbox(this.imagesSrc[index + 1], this.images[index + 1].nodeName, this.alt[index + 1]);
  }

  handleSlideButtons = () => {
    document.querySelector('#next').addEventListener('click', this.goNext);
    document.querySelector('#prev').addEventListener('click', this.goPrev);
  }

  handleSlideKeys = () => {
    document.addEventListener('keyup', this.onKeyUp);
  }

  /**
   * Create Img or Video
   * @param { String } src 
   * @param { String } nodeName 
   * @param { String } alt 
   */
  createImage = (src, nodeName, alt) => {
    this.src = null;
    // const image = new Image();
    // image.alt = alt;
    // image.className = 'lightbox__img';
    // image.setAttribute('tabindex', '0');
    // const video = document.createElement('video');
    // video.setAttribute('controls', '');
    // video.setAttribute('alt', alt);
    // video.setAttribute('type', 'video/mp4');
    // video.classList.add('lightbox__img');
    // video.setAttribute('tabindex', '0')



    if (nodeName === 'VIDEO') {
      const video = new MediaFactory('video').render();
      this.lightboxContainer.innerHTML += video;
      const domVideo = document.querySelector('.lightbox__img');
      this.src = src;
      domVideo.src = src;
      domVideo.setAttribute('alt', alt);
      domVideo.focus();
    } else if (nodeName === 'IMG') {
      const image = new MediaFactory('image').render();
      this.lightboxContainer.innerHTML += image;
      const domImage = document.querySelector('.lightbox__img');
      this.src = src;
      domImage.setAttribute('src', src);
      domImage.setAttribute('alt', alt);
      domImage.focus();
    }
  }

  /**
  *   @param {KeyboardEvent} e 
  */
  onKeyUp(e) {
    e.stopImmediatePropagation();

    if (e.key === 'Escape') {
      this.closeLightbox(e);
    } else if (e.key === 'ArrowLeft') {
      this.goPrev(e);
    } else if (e.key === 'ArrowRight') {
      this.goNext(e);
    }
  }

  render = () => {
    const images = Array.from(document.querySelectorAll('.header__image'));
    this.imagesSrc = images.map(image => image.getAttribute('src'));
    this.alt = images.map(image => image.getAttribute('alt'));
    images.forEach(image => {
      let alt = image.alt;

      image.addEventListener('click', e => {
        e.preventDefault();
        this.src = e.currentTarget.getAttribute('src');
        this.nodeName = e.target.nodeName;
        this.openLightbox(this.src, this.nodeName, alt);
      });

      image.onkeyup = (e) => {
        if (e.key === 'Enter') {
          this.src = e.currentTarget.getAttribute('src');
          this.nodeName = e.target.nodeName;
          this.openLightbox(this.src, this.nodeName, alt);
        }
      }
    });


  }

  createLightboxHtmlElements() {
    const template = document.createElement('div');
    template.classList.add('lightbox__container');
    template.setAttribute('role', 'dialog');
    template.setAttribute('aria-modal', false);
    template.setAttribute('aria-hidden', true);
    template.setAttribute('tabindex', '-1');
    template.innerHTML = `
        <div class="close lightbox__closeIcon"><span class="icon fas fa-times"></span></div>
        <button id="prev" class="lightbox__btn left" aria-label="précédent"><i class="fas fa-chevron-left"></i></i></button> 
        <button id="next" class="lightbox__btn right" aria-label="suivant"><i class="fas fa-chevron-right"></i></i></button>
    `;
    return document.querySelector('#main').appendChild(template);
  }
}

/**
 *  Class representing a contact form html template
 */
class ContactForm {
  constructor() {
    this.section = document.querySelector('.section');
    this.aside = document.querySelector('aside');
    this.form = document.querySelector('.dialog');
    this.modal = document.querySelector('#contactModal');
    this.formBody = document.querySelector('#form');
    this.inputs = document.querySelectorAll('input');
    this.textarea = document.querySelector('textarea');
    this.openButton = document.querySelector('.contact__button');
    this.closeButton = document.querySelector('.btn-close');
    this.focusEls = 'button, input, textarea';
    this.focusables = [];
    this.focusedElementBeforeModal;
    this.firstTab;
    this.lastTab;
  }

  /**
   * @param { Event } e 
   */
  onSubmit = (e) => {
    e.preventDefault();
    this.inputs.forEach(el => {
      console.log(el.value);
    });
    console.log(this.textarea.value);
    this.removeForm();
  }

  /**
   * @param { KeyboardEvent } e 
   */
  trapTabKey = (e) => {
    if (e.key === 'Tab') {
      if (e.shiftKey) {
        if (document.activeElement === this.firstTab) {
          e.preventDefault();
          this.lastTab.focus();
        }
      } else {
        if (document.activeElement === this.lastTab) {
          e.preventDefault();
          this.firstTab.focus();
        }
      }
    }

    const index = this.focusables.indexOf(document.activeElement);
    let nextIndex = 0;

    if (e.key === 'ArrowUp') {
      e.preventDefault();
      nextIndex = index > 0 ? index - 1 : 0;
      this.focusables[nextIndex].focus();
    }
    if (e.key === 'ArrowDown') {
      e.preventDefault();
      nextIndex = index + 1 < this.focusables.length ? index + 1 : index;
      this.focusables[nextIndex].focus();
    }
  }
  /**
  * @param { KeyboardEvent } e 
  */
  onKeyUp = (e) => {
    if (e.key === 'Escape') {
      this.removeForm();
    }
  }

  closeLightboxKey = () => {
    document.addEventListener('keyup', this.onKeyUp);
  }

  removeForm = () => {
    this.form.style.display = 'none';
    this.modal.setAttribute('aria-modal', false);
    this.modal.setAttribute('aria-hidden', true);
    this.section.setAttribute('aria-hidden', false);
    this.aside.setAttribute('aria-hidden', false);
    document.removeEventListener('keyup', this.onKeyUp);
    document.removeEventListener('keydown', this.trapTabKey);
    this.formBody.removeEventListener('submit', this.onSubmit);
    this.openButton.focus();
  }

  openContactForm = () => {

    this.form.style.display = 'flex';
    this.form.classList.add('active');

    this.modal.setAttribute('aria-modal', true);
    this.modal.setAttribute('aria-hidden', false);

    this.section.setAttribute('aria-hidden', true);
    this.aside.setAttribute('aria-hidden', true);

    this.focusedElementBeforeModal = document.activeElement;
    document.addEventListener('keydown', this.trapTabKey);
    this.focusables = Array.from(this.modal.querySelectorAll(this.focusEls));
    this.firstTab = this.focusables[0];
    this.lastTab = this.focusables[this.focusables.length - 1];
    this.firstTab.focus();

    this.formBody.addEventListener('submit', this.onSubmit);
  }

  closeContactForm = () => {
    this.closeButton.addEventListener('click', this.removeForm);
  }

  render = () => {
    this.openButton.addEventListener('click', () => {
      this.openContactForm()
      this.closeContactForm();
      this.closeLightboxKey();
    });
  }

}

/**
 *  Class Factory lightbox / contact
 */
class ModalFactory {
  constructor(type) {
    switch (type) {
      case 'lightbox': return new Lightbox();
      case 'contactForm': return new ContactForm();
    }
  }
}

export default ModalFactory;