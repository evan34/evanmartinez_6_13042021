/**
 *  Class representing a phototrapher card info 
 */
class PhotographerInfo {
  /**
   * 
   * @param { String } likes 
   * @param { String } price 
   */
  constructor(likes, price) {
    this.likes = likes;
    this.price = price;
  }

  render() {
    const totalLikes = this.likes;
    return `
      <aside id="photographer-info" class="photographer-info" aria-hidden="false">
        <p class="totalLikes">${totalLikes.reduce((a, b) => a + b, 0)}</p> 
        <i class="fas fa-heart"></i>      
        <p class="priceByDay">${this.price}€ / jour</p>
      </aside>
    `;
  }
}

export default PhotographerInfo;